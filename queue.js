  let collection = [];
  
// Write the queue functions below.
 
  function print() {
   	return collection;
  }
	
  function enqueue(data) {
    collection.push(data);
  }

  function dequeue() {
    return collection.shift();
  }

  function front() {
    return collection[0];
  }

  function isEmpty() {
    return collection.length === 0;
  }

  function size() {
    return collection.length;
  }

  



module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};